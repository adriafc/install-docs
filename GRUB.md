### Install grub-customizer
Easy to set basic parameters like entry names, entry order, max timeout, default selection, ...
```
sudo add-apt-repository ppa:danielrichter2007/grub-customizer
sudo apt-get install grub-customizer
```

Every time you do a change with grub-customizer, it will overwritte any change done in your 'grub.cfg'.

### grub basic theme
**Repository:** https://github.com/vinceliuice/grub2-themes

### Add an icon to an entry
Locate in the file '/boot/grub/grub.cfg' your GRUB entries and add:
```
--class *icon_name* (Without extension)
```
**Before:**
```
### BEGIN /etc/grub.d/31_linux_proxy ###
menuentry "Elementary" $menuentry_id_option 
...
### END /etc/grub.d/31_linux_proxy ###
```
**After:**
```
### BEGIN /etc/grub.d/31_linux_proxy ###
menuentry "Elementary" --class icon_name $menuentry_id_option 
...
### END /etc/grub.d/31_linux_proxy ###
```

