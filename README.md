## Elementary Install Documentation
This is a elementary pre/post installation guide to get the basic features:

+   [GRUB](./GRUB.md)
+   [Graphics](./GRAPHICS.md)
