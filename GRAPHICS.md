### Nvidia Graphics
#### Method 1
Execute:
```
sudo ubuntu-drivers autoinstall
```

#### Method 2 (not tested)
Execute:
```
sudo ubuntu-drivers devices
sudo apt install nvidia-driver-xxx (E.g. nvidia-driver-450)
```

After installing, to preserve the changes after boot:
```
sudo nvidia-x-server
```

#### Save settings after boot
Set the proper configuration.
Open displays settings, modify whatever and apply... -.-

### Enable >60hz animations for Window Manager
Edit the file '/etc/environment' and add the following lines:
```
CLUTTER_DEFAULT_FPS=144   #Put your refresh rate
__GL_SYNC_DISPLAY_DEVICE=DVI-D-0 #Mine was DP-0
```

### Enable add-apt-repository command
Execute:

```
sudo apt-get install software-properties-common

```
### Tweaks
Execute:
```
sudo add-apt-repository ppa:philip.scott/elementary-tweaks
sudo apt install elementary-tweaks
```

### Login screen resolution, orientation, ...
Execute:
```
sudo apt install lightdm-gtk-greeter-settings
```

All the settings selected in lightdm-gtk-greeter-settings are in:
```
/etc/lightdm/lightdm-gtk-greeter.conf
```

Create the file '/etc/lightdm/lightdm.conf', set the permissions and add the following content:
```
[Seat:*]
greeter-session=lightdm-gtk-greeter
display-setup-script=/opt/login-screen-conf.sh
```

Create the file '/opt/login-screen-conf.sh', set the permissions and add the following content:
```
#!/usr/bin/env sh
xrandr --output DVI-D-0 --mode 1920x1080 --rate 144
xrandr --output HDMI-0 --mode 1920x1080 --rate 60 --rotate right
```
**NOTE: This is my config. Adapt it to your preferences. Check xrandr docs.**

Modify the file '/etc/gtk-3.0/settings.ini' and add the following lines:
```
gtk-cursor-theme-name = elementary
gtk-cursor-theme-size = 16
```

**NOTE: I don't know if you need to install the tweaks, or some theme to have this.**

### Theme
#### Mojave GTK Theme
**Repository:** htts://github.com/vinceliuice/Mojave-gtk-theme

#### Installing icon set won't login
If after selecting the icon set, the GUI gets stuck at login, remove the icon set in:
```
/usr/share/icons/
```

I fixed an icon set calling 'chmod -R 755 ~/path_icon_set/*' before installing.

